// Function for plotting a graph with the data provided. See https://quickchart.io/documentation/ for more options
module.exports = (gastos) => {
  // Graph Library
  const QuickChart = require('quickchart-js');
  const myChart = new QuickChart();
  myChart
  .setConfig({
    "type": "outlabeledPie",
    "data": {
      "labels": Object.keys(gastos),
      "datasets": [{
        "data": Object.values(gastos),
        backgroundColor: ['#6388b4', '#ffae34', '#ef6f6a', '#8cc2ca', '#55ad89', '#c3bc3f', '#bb7693', '#baa094', '#a9b5ae', '#767676'],
      }]
    },
    "options": {
      title: {
        display: true,
        text: 'Gastos del Mes',
        fontSize: 32
      },
      "plugins": {
        "legend": false,
        "outlabels": {
          "text": "%l %p",
          "color": "white",
          "stretch": 35,
          "font": {
            "resizable": true,
            "minSize": 12,
            "maxSize": 18
          }
        }
      }
    }
  })
  .setWidth(750)
  .setHeight(750)
  .setBackgroundColor('transparent');
  
  // Print the chart URL
  return myChart.getUrl();
}