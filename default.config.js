// Rename this to `config.js` and modify all the variables!
module.exports = {
  // Configurar URL de replit. Esto es para que Telegram sepa a donde apuntar (no importa si el bot es LOCAL)
  BOTURL: 'https://replitname.username.repl.co',
  
  // Configurar Token de Telegram bot
  TOKEN: "REPLACE TOKEN",
  
  // Current Month View Name
  CURRENT_MONTH_VIEW: "REPLACE with Airtable Current Month View",
  
  // Configurar apiKey y base de Airtable
  ATAPIKEY: "REPLACE API",
  ATBASEKEY: "REPLACE BASE",
  
  // Restringir a los siguientes usuarios
  ADMINS: [ADMINIDs],
  
  // Configurar las categorías (tendrían que ser las mismas que en Airtable)
  BOTONES: [[
    {text: "🏪 Mercado", callback_data: "Mercado"},
    {text: "👨‍👩‍👧‍👦 Chicos", callback_data: "Chicos"},
    {text: "🏡 Casa", callback_data: "Casa"},
    {text: "👨🏻‍💻 Maco", callback_data: "Maco"}
  ],[
    {text: "🦸🏻‍♀️ Flor", callback_data: "Flor"},
    {text: "🍫 Vicios", callback_data: "Vicios"},
    {text: "🥫 Comida", callback_data: "Comida"},
    {text: "🧹 Limpieza", callback_data: "Limpieza"}
  ]],
    
  // Para ejecutar localmente
  LOCAL: false,
}