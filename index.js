// ------ Configuration ------- //
var config = require('./config.js');

// ------ Plot Graph options ------- //
var plotGraph = require('./plotGraph.js');

// ------ Capitalize Function ------- //
String.prototype.capitalizeFirstLetter = function() {
  return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
}

// New Bot
const TelegramBot = require('node-telegram-bot-api');

var bot;
var ultimo_id;

if(config.LOCAL) {
  console.log("LOCAL")
  bot = new TelegramBot(config.TOKEN, { polling: true });
} else {
  bot = new TelegramBot(config.TOKEN);
  // This informs the Telegram servers of the new webhook.
  bot.setWebHook(`${config.BOTURL}/bot${config.TOKEN}`);

  // New Server
  const express = require('express');
  const app = express();

  // parse the updates to JSON
  app.use(express.json());

  app.get('/', (req, res) => {
    res.send('<pre>Nothing to see here!</pre>');
  });

  // We are receiving updates at the route below!
  app.post(`/bot${config.TOKEN}`, (req, res) => {
    bot.processUpdate(req.body);
    res.sendStatus(200);
  });

  // Start Express Server
  const port = 11234
  app.listen(port, () => {
    console.log(`Express server is listening on ${port}`);
  });
}

// Connect to the Airtable API
var Airtable = require('airtable');
var base = new Airtable({apiKey: config.ATAPIKEY}).base(config.ATBASEKEY);

// Listens for messages to the bot
bot.on('message', (msg) => {
  var chatId = msg.chat.id;
  mensaje = msg.text;
  const opts = {
    parse_mode: 'Markdown',
    reply_to_message_id: msg.message_id,
    reply_markup: JSON.stringify({
      inline_keyboard: config.BOTONES
    })
  };

  // Checkear que sea un admin el que mande el mensaje
  if(config.ADMINS.includes(chatId)) {
    if(mensaje.includes('/resumen')) {
      // Normally, Airtable columns have names with the first letter in uppercase, we're making sure this is the case.
      var categoria = mensaje.slice(mensaje.lastIndexOf(" ") + 1).capitalizeFirstLetter();
      var hayRecords = false;
      var montoTotal = 0;
      base('Gastos').select({
        view: config.CURRENT_MONTH_VIEW,
        filterByFormula: "{Categoria} = '" + categoria + "'"
      }).eachPage(function page(records, fetchNextPage) {
          if(records.length > 0) {hayRecords = true}
          records.forEach(function(record) {
            montoTotal += record.get('Monto');
          });     
          fetchNextPage();
      }, function done(err) {
          if(hayRecords) {
            montoTotal = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(montoTotal);
            bot.sendMessage(chatId, 'El total de gastos de ' + categoria + ' de este mes es: ' + montoTotal)
          } else {
            bot.sendMessage(chatId, 'La categoría no es válida');
          }
          if (err) { 
            bot.sendMessage(chatId, `❌ Hubo un error:${err}`);
            console.error(err); 
            return; 
          }
      });
    } else if(mensaje.includes('/grafico')) {
      var gastos = [];
      base('Gastos').select({
        view: config.CURRENT_MONTH_VIEW,
      }).eachPage(function page(records, fetchNextPage) {
          records.forEach(function(record) {
            if(gastos[record.get('Categoria')]==undefined) {gastos[record.get('Categoria')] = 0}
            gastos[record.get('Categoria')] += parseFloat(record.get('Monto'));
          });     
          fetchNextPage();
      }, function done(err) {
          //console.log(gastos);
          // Render Graph (see plotGraph.js for options)
          bot.sendPhoto(chatId, plotGraph(gastos))
          if (err) { 
            bot.sendMessage(chatId, `❌ Hubo un error:${err}`);
            console.error(err); 
            return; 
          }
      });
    } else {
      // Checkear que el mensaje tenga un monto
      var digitos = /\d+/;
      if((mensaje.match(digitos) != null)) {
        bot.sendMessage(chatId, '¿Qué categoría?', opts)
      } else {
          bot.sendMessage(chatId, 'El formato es `[descripcion] [monto]`', {parse_mode: 'Markdown'});
      }
    }
  }
  
});

// Edit last message
bot.on('edited_message', (msg) => {
  mensaje = msg.text;
  var digitos = /\d+/;
    if((mensaje.match(digitos) != null)) {
      var gasto = mensaje.slice(0, mensaje.lastIndexOf(" "));
      var monto = Number(mensaje.slice(mensaje.lastIndexOf(" ")));
      base('Gastos').update([
        {
          "id": ultimo_id,
          "fields": {
            "Gasto": gasto,
            "Monto": monto,
          }
        }
      ], function(err, records) {
        if (err) {
          console.error(err);
          return;
        }
        records.forEach(function(record) {
          console.log(record.get('Monto'));
        });
      });
    }
});

bot.on('callback_query', (msg) => {
  //console.log(msg)

  var mensajeOriginal = msg.message.reply_to_message.text;

  var fecha = new Date().toISOString();
  var gasto = mensajeOriginal.slice(0, mensajeOriginal.lastIndexOf(" "));
  var monto = Number(mensajeOriginal.slice(mensajeOriginal.lastIndexOf(" ")));
  var categoria = msg.data;
  var editedText = "✅ Cargado en *" + categoria + '*';
  
  const opts = {
    chat_id: msg.message.chat.id,
    message_id: msg.message.message_id,
    parse_mode: 'Markdown'
  };
  
  // Crear el record en la base de Airtable
  base('Gastos').create([
    {
      "fields": {
        "Date": fecha,
        "Gasto": gasto,
        "Monto": monto,
        "Categoria": categoria
      }
    }
  ], function(err, records) {
    // Función que corre después que se crea el record (o nó)
    if (err) {
      bot.answerCallbackQuery(msg.id, "Error!");
      bot.editMessageText("❌ Hubo un error en la carga: `" + err + '`', opts);
      console.error(err);
      return;
    }
    records.forEach(function (record) {
      //console.log(record.getId())
      ultimo_id = record.getId();
      // Banner de arriba
      bot.answerCallbackQuery(msg.id, "Gasto creado!");
      // Edita el mensaje del bot y saca los botones
      bot.editMessageText(editedText, opts);
    });
  });
})
